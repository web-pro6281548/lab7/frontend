import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import { useLoadingStore } from './loading'
import userService from '@/services/user'
import type { User } from '@/types/User'

export const useUserStore = defineStore('user', () => {
  const loadingStone = useLoadingStore()
  const users = ref<User[]>([])
  async function getUser(id: number) {
    loadingStone.doLoad()
    const res = await userService.getUser(id)
    users.value = res.data
    loadingStone.finish()
  }
  async function getUsers() {
    loadingStone.doLoad()
    const res = await userService.getUsers()
    users.value = res.data
    loadingStone.finish()
  }

  async function saveUser(user: User) {
    loadingStone.doLoad()
    if (user.id < 0) {
      console.log('Post' + JSON.stringify(user))
      //add new
      const res = await userService.addUser(user)
    } else {
      console.log('Patch' + JSON.stringify(user))
      //update
      const res = await userService.updateUser(user)
    }
    await getUsers()
    loadingStone.finish()
  }

  async function deleteUser(user: User) {
    loadingStone.doLoad()
    const res = await userService.delUser(user)
    await getUsers()
    loadingStone.finish()
  }

  return { users, getUsers, saveUser, deleteUser, getUser }
})
